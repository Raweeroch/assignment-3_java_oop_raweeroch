package oop;

public class Computer implements NumComputer {

	public String titleCom;
	public int NumCom;
	
	public Computer(String titleComInsert, int NumcomInsert) {
		this.NumCom = NumcomInsert;
		this.titleCom = titleComInsert;
	}

	@Override
	public String getTitleCom() {
		return titleCom;
	}

	@Override
	public int getNumCom() {
		return NumCom;
	}

	@Override
	public String toString() {
		return " Number" + NumCom + "  =  " + titleCom;
	}
	
}
